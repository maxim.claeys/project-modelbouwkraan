#include <ESP8266WiFi.h>

const char *ssid = "DESKTOP";
const char *password = "1r7X74-2";

int LED = D0; 
int magneet = D1;
int hijsmotor = D2;
int zakmotor = D3;
int draaimotorL = D4;
int draaimotorR = D5;
//int voorwaarts = D6;
//int achterwaarts = D7;
//int arm_hijsen = D8;
//int arm_zakken = D9;

WiFiServer server(80);

void setup(){
  Serial.begin(115200); // Default Baudrate

  Serial.print("Connecting to the Network");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  server.begin(); // Starts the Server
  Serial.println("Server started");

  Serial.print("IP Address of network: "); // will IP address on Serial Monitor
  Serial.println(WiFi.localIP());
}

void loop()
{
  WiFiClient client = server.available();
  if (!client)
  {
    return;
  }
  //Serial.println("Waiting for new client");
  while (!client.available())
  {
    delay(1);
  }

  String request = client.readStringUntil('\r');
  //Serial.println(request);
  client.flush();

  if (request.indexOf("/COMMAND=licht_aan") != -1){
    digitalWrite(LED, HIGH);
  }
  if (request.indexOf("/COMMAND=licht_uit") != -1){
    digitalWrite(LED, LOW);
  }
  if (request.indexOf("/COMMAND=magneet_aan") != -1){
    digitalWrite(magneet, HIGH);
  }
  if (request.indexOf("/COMMAND=magneet_uit") != -1){
    digitalWrite(magneet, LOW);
  }
  if (request.indexOf("/COMMAND=hijsen") != -1){
    digitalWrite(hijsmotor, HIGH);
    delay(200);
    digitalWrite(hijsmotor, LOW);
  }
  if (request.indexOf("/COMMAND=zakken") != -1){
    digitalWrite(zakmotor, HIGH);
    delay(200);
    digitalWrite(zakmotor, LOW);
  }
  if (request.indexOf("/COMMAND=links") != -1){
    digitalWrite(draaimotorL, HIGH);
    delay(200);
    digitalWrite(draaimotorL, LOW);
  }
  if (request.indexOf("/COMMAND=rechts") != -1){
    digitalWrite(draaimotorR, HIGH);
    delay(200);
    digitalWrite(draaimotorR, LOW);
  }
  // if (request.indexOf("/COMMAND=voorwaarts") != -1){
  //   digitalWrite(voorwaarts, HIGH);
  //   delay(200);
  //   digitalWrite(voorwaarts, LOW);
  // }
  // if (request.indexOf("/COMMAND=achterwaarts") != -1){
  //   digitalWrite(achterwaarts, HIGH);
  //   delay(200);
  //   digitalWrite(achterwaarts, LOW);
  // }
  // if (request.indexOf("/COMMAND=arm_hijsen") != -1){
  //   digitalWrite(arm_hijsen, HIGH);
  //   delay(200);
  //   digitalWrite(arm_hijsen, LOW);
  // }
  // if (request.indexOf("/COMMAND=arm_zakken") != -1){
  //   digitalWrite(arm_zakken, HIGH);
  //   delay(200);
  //   digitalWrite(arm_zakken, LOW);
  // }

  //*------------------HTML Page Code---------------------*//
  client.println("HTTP/1.1 200 OK"); //
  client.println("Content-Type: text/html");
  client.println("");
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>");

  client.println("<style>");
  client.println(".container {position: relative; width: fit-content;}");
  client.println("img {width: auto; max-height: 90vh; object-fit: contain;}");
  client.println(".btn{font-size: 2vh;}");
  client.println(".container .btn-primary {position: absolute; transform: translate(-50%, -50%); -ms-transform: translate(-50%, -50%);}");
  client.println(".hijsen {top: 15%; left:20%;}");
  client.println(".zakken {top: 30%; left: 20%;}");
  client.println(".links {top: 85%; left: 45%}");
  client.println(".rechts {top: 85%; left: 72%}");
  client.println(".voorwaarts {top: 95%; left: 20%;}");
  client.println(".achterwaarts {top: 95%; left: 80%;}");
  client.println(".arm-hijsen {top: 15%; left: 70%;}");
  client.println(".arm_zakken {top: 30%; left: 70%;}");
  client.println("</style>");

  client.println("<body>");
  client.println("<div class='d-flex justify-content-center container'>");
  client.println("<img src='https://i.imgur.com/ppcoFYj.jpg'>");
  client.println("<a class='btn btn-primary hijsen' href='/COMMAND=hijsen'>hijsen</a>");
  client.println("<a class='btn btn-primary zakken' href='/COMMAND=zakken'>zakken</a><br />");
  client.println("<a class='btn btn-primary links' href='/COMMAND=links'>links</a>");
  client.println("<a class='btn btn-primary rechts' href='/COMMAND=rechts'>rechts</a><br />");
  //client.println("<!-- <a class='btn btn-primary voorwaarts' href='/COMMAND=voorwaarts'>voorwaarts</a>");
  //client.println("<a class='btn btn-primary achterwaarts' href='/COMMAND=achterwaarts'>achterwaarts</a><br /> -->");
  //client.println("<a class='btn btn-primary arm_hijsen' href='/COMMAND=arm_hijsen'>arm_hijsen</a>");
  //client.println("<a class='btn btn-primary arm_zakken' href='/COMMAND=arm_zakken'>arm_zakken</a><br />");


  client.println("</div>");
  client.println("<div class='d-flex justify-content-center'>");
  client.println("<div class='btn-group' role='group'>");
  client.println("<a class='btn btn-primary' href='/COMMAND=licht_aan'>licht_aan</a>");
  client.println("<a class='btn btn-primary' href='/COMMAND=licht_uit'>licht_uit</a><br />");
  client.println("</div>");

  client.println("<div class='btn-group' role='group'>");
  client.println("<a class='btn btn-primary' href='/COMMAND=magneet_aan'>magneet_aan</a>");
  client.println("<a class='btn btn-primary' href='/COMMAND=magneet_uit'>magneet_uit</a><br />");
  client.println("</div>");
  client.println("</div>");
  client.println("</body>");

  client.println("</html>");

  delay(1);
  //Serial.println("Client disonnected");
  Serial.println("");
}
